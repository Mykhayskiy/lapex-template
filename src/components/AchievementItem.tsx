import React from "react";
import styles from "../styles/BlockAchivements.module.css";

interface AchivementItemParams {
    achievementName: string;
    value: number;
    title: string;
}

const AchievementItem = ({achievementName, value, title}: AchivementItemParams) => {
    return (
        <>
            <div className={`${styles.achievementItemWrapper} ${styles[achievementName]}`}>
                <div className={styles.achievementItem}>
                    <div className={styles.achievementItemInner}>
                        <div className={styles.achievementValue}>{value}</div>
                        <div className={styles.achievementTitle}>{title}</div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default AchievementItem;