import React from "react";
import { Helmet } from "react-helmet";
import styles from "../styles/pageErrors.module.css";

const NotFound = () => {

    <Helmet>
        <title>Title using helmet</title>
    </Helmet>

    function toFront() {
        window.location.href = "/";
    }
    setTimeout(toFront, 5000);

    return(
        <>
            <div className={"pageSection"}>
            <div className={`${styles.PageErrors} ${styles.page404}`}>
                <span className={styles.errorNumber}>404</span>
                <div className={styles.errorTitle}>Sorry, the page you are looking for<br />could not be found</div>
            </div>
                <div className={styles.serviceText}>You will be redirected to the <a href={"/"}>front page</a> in 5 seconds.</div>
            </div>
        </>
    );
}
export default NotFound;