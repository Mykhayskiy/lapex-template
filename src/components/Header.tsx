import React from "react";
import MainMenu from "./MainMenu";
import styles from "../styles/Header.module.css";

const Header = () => {
    return (
        <header className={styles.header}>
            <MainMenu/>
        </header>
    );
}
export default Header;