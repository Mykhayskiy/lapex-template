import React from "react";
import {Helmet} from "react-helmet";
import styles from "../styles/page.module.css";

const Contacts = () => {
    return (
        <>
            <Helmet>
                <title>Contact Us | Lapex - Scaffolding</title>
                <meta name="description" content="Contact Us"/>
            </Helmet>

            <div className={styles.section}>
                <div className={styles.pageInner}>
                    <h1 className={styles.pageTitle}>Contact Us</h1>

                    Thank you for considering us for your scaffolding needs. We value your interest and
                    are here to assist you with any inquiries or information you may require.
                    Our head office is in Poland (Gdansk), but we have representatives and partners in Czechia, Belgium and Germany.<br />
                    There are several ways you can reach us:

                    <h3>Contact Information</h3>
                    <br />

                    Address:<br />
                    [Your Company Address]<br />

                    Phone:<br />
                    [Your Company Phone Number]<br />

                    Email:<br />
                    [Your Company Email Address]<br />

                    Business Hours<br />

                    Our dedicated team is available during the following business hours:<br />

                    Monday - Friday:<br />
                    07:30 - 17:30<br />

                    Saturday, Sunday:<br />
                    Closed
                </div>
            </div>
        </>
    );
}

export default Contacts;