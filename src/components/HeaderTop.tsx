import React from "react";
import styles from "../styles/HeaderTop.module.css";

const HeaderTop = () => {
    return(
        <div id={styles.headerTop}>

            <div className={styles.HeaderTopText}>
                Labour Hire: +48 123 45 67 | Sales: +48 345 56 78
            </div>

            <div className={styles.HeaderTopHiring}>
                WE ARE HIRING! Give Us a Call +48 123 45 67
            </div>

            <div className={styles.HeaderTopPhones}>
                info@lapex.waw.pl
            </div>
        </div>
    );
}
export default HeaderTop;