import React from "react";
import styles from "../styles/BlockAchivements.module.css";
import AchievementItem from "./AchievementItem";

interface BlockAchievementsParams {
    title: string;
    sectionColor: string;
    blockType: string;
}

const BlockAchievements = ({title, sectionColor, blockType}: BlockAchievementsParams) => {
    return (
        <>
            <div className={`section ${sectionColor} ${blockType} ${styles.blockAchievementsWrapper}`}>
                <div className={styles.blockTitle}><h1>{title}</h1></div>
                <div className={styles.achievementsInner}>

                    <div className={styles.achievementsList}>
                        <AchievementItem achievementName={"grip"} value={45} title={"Reliable Partners"}/>
                        <AchievementItem achievementName={"projects"} value={78} title={"Projects Completed"}/>
                        <AchievementItem achievementName={"years"} value={12} title={"Years in Business"}/>
                        <AchievementItem achievementName={"offices"} value={4} title={"Locations in EU"}/>
                    </div>

                    <div className={styles.achievementsDescription}>
                        <div className={styles.descTitle}>12 Years Experience</div>
                        <div className={styles.descText}>Our company has been the<br />leading provided construction<br />services to clients throughout<br />the Europe since 2011.</div>
                        <div><a className={`btn btn-primary`} href={"/contacts"}>Contact Us</a></div>
                    </div>

                </div>
            </div>
        </>
    );

}
export default BlockAchievements;