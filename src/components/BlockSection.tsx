import React from "react";
import styles from "../styles/BlockSection.module.css";
import ServicesCard from "./ServicesCard";

interface BlockSectionParams {
    title: string;
    sectionColor: string;
    blockType: string;
}

const BlockSection = ({title, sectionColor, blockType}: BlockSectionParams) => {
    return (
      <>
          <div className={`section ${sectionColor} ${blockType} ${styles.blockSectionWrapper}`}>
          <div className={styles.blockSection}>
              <div className={styles.blockTitle}><h1>{title}</h1></div>

              <div className={styles.box}>
              <ServicesCard title={"Construction"} blockType={"construction"}/>
              </div>

              <div className={styles.box}>
                  <ServicesCard title={"Renovation"} blockType={"renovation"}/>
              </div>

              <div className={styles.box}>
                  <ServicesCard title={"Consultation"} blockType={"consultation"}/>
              </div>
          </div>
          </div>
      </>
    );

}
export default BlockSection;