import React from "react";
import styles from "../styles/Footer.module.css";

const Footer = () => {
    return (
        <>
            <footer id={styles.footer}>
                <div className={styles.footerInner}>
                    <div className={styles.footerLeft}>
                        <div className={styles.footerItem}>
                            <span className={styles.footerItemLabel}>Head Office:</span><span className={styles.footerItemText}>6391 Elgin St. Celina, Delaware 10299</span>
                        </div>
                        <div className={styles.footerItem}>
                            <span className={styles.footerItemLabel}>Phone:</span>
                            <span className={`${styles.footerItemText} ${styles['phones']}`}>+84 1102 2703<br />+84 1102 2703<br />+84 1102 2703</span>
                        </div>
                        <div className={styles.footerItem}>
                            <span className={styles.footerItemLabel}>Email:</span><span className={styles.footerItemText}>info@lapex.waw.pl</span>
                        </div>

                        <div className={`${styles.footerItem} ${styles["socialIcons"]}`}>
                            <span className={styles.footerItemLabel}>Social:</span>
                            <span className={styles.footerItemSocialLinks}>
                                <a href={"#"}><img src={"/images/fb.svg"}/></a>
                                <a href={"#"}><img src={"/images/linkedin.svg"}/></a>
                            </span>
                        </div>

                        <div className={styles.footerLogo}><a href={"/"}><img src={'/images/logoLapex.svg'} width={'300px'}/></a></div>
                    </div>

                    <div className={styles.footerRight}>
                        <span className={styles.footerItemLabel}>We work all over the Europe</span>
                        <div className={styles.footerMap}>
                            <img src={"/images/lapexMap.png"}/>
                        </div>
                    </div>

                </div>
                <div className={styles.footerBottomLine}>
                    <div className={styles.footerBottomLineInner}>
                        Lapex Inc. &copy; 2011
                    </div>
                </div>
            </footer>
        </>
    )
}
export default Footer;