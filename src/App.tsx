import React from 'react';
import './App.css';
import {Routes, Route, Link} from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import NotFound from "./pages/NotFound";
import Header from "./components/Header";
import Footer from "./components/Footer";
import './styles/global.css';
import HeaderTop from "./components/HeaderTop";
import {Helmet} from "react-helmet";
import Services from "./pages/Services";
import Contacts from "./pages/Contacts";

function App() {
  return (
    <div className="App">

        <Helmet>
            <meta charSet="utf-8" />
            <title>Scaffolding and services | Lapex</title>
        </Helmet>

        <HeaderTop />
        <Header />
        <div className={'main'}>
        <Routes>
            <Route path={'/'} element={<Home/>}/>
            <Route path={'/about'} element={<About/>} />
            <Route path={'/services'} element={<Services/>} />
            <Route path={'/contacts'} element={<Contacts/>} />
            <Route path={'*'} element={<NotFound/>} />
        </Routes>
        </div>
        <Footer />
    </div>
  );
}

export default App;
