import React from "react";
import styles from '../styles/HeroSection.module.css';

const HeroSection = () => {
    return (
        <>
            <div className={styles.heroSection}>
                <div className={styles.heroContainerWrapper}>
                    <div className={styles.heroContainer}>
                        <div className={styles.heroTitle}><span>Lapex Polska</span></div>
                        <div className={styles.heroDescription}><span>Wynajem rusztowań i robociznę</span></div>
                        <a href={'/contacts'} className={`btn`}>Contact us</a>
                    </div>
                </div>
            </div>
        </>
    );
}
export default HeroSection;