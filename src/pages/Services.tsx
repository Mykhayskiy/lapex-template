import React from "react";
import {Helmet} from "react-helmet";
import styles from "../styles/page.module.css";
import CtaBlock from "../components/CtaBlock";

const Services = () => {
    return (
        <>
            <Helmet>
                <title>Services | Lapex - Scaffolding</title>
                <meta name="description" content="Services"/>
            </Helmet>

            <div className={styles.section}>
                <div className={styles.pageInner}>
                    <h1 className={styles.pageTitle}>Our Services</h1>
                    At Lapex Inc., we offer a comprehensive range of scaffolding services designed to meet the diverse
                    needs of construction, maintenance, and repair projects. Our commitment to excellence and safety
                    sets us apart, making us the preferred choice for businesses seeking reliable scaffolding solutions.
                    Explore our services below:
                    <br/>
                    <br/>
                    <h3>1. Scaffolding Installation</h3>

                    <h4 className={styles.sectionH4}>Tailored Solutions</h4>

                    We specialize in providing customized scaffolding solutions to suit the unique requirements of each
                    project. Our experienced team works closely with clients to design and install scaffolding
                    structures that ensure a safe and efficient working environment at elevated heights.

                    <h4 className={styles.sectionH4}>Safety First</h4>

                    Prioritizing the safety of our clients and workers, our scaffolding installations adhere to the
                    highest safety standards. We implement rigorous checks and inspections to guarantee a secure
                    platform for your construction tasks.

                    <h3>2. Scaffolding Removal</h3>

                    <h4 className={styles.sectionH4}>Swift and Safe Dismantling</h4>

                    Once your project is complete, rely on us for prompt and safe scaffolding removal. Our trained
                    professionals dismantle structures with precision, ensuring a smooth transition and minimal
                    disruption to your operations.

                    <h4 className={styles.sectionH4}>Disposal and Recycling</h4>

                    We are committed to environmental responsibility. Our scaffolding removal services include proper
                    disposal and recycling of materials, minimizing the environmental impact of your project.

                    <h3>3. Inspection and Maintenance</h3>

                    <h4 className={styles.sectionH4}>Regular Inspections</h4>

                    Ensure the ongoing safety and stability of your scaffolding structures with our inspection and
                    maintenance services. Our team conducts regular inspections to identify and address any issues
                    promptly.

                    <h4 className={styles.sectionH4}>Repairs and Upgrades</h4>

                    In addition to inspections, we offer repair and upgrade services to enhance the longevity and
                    performance of your scaffolding. Trust us to keep your structures in optimal condition.

                    <h3>4. Consultation and Design</h3>

                    <h4 className={styles.sectionH4}>Project Assessment</h4>

                    Our experts provide project assessment and consultation services to determine the most suitable
                    scaffolding solution for your specific needs. We consider factors such as project scope, timelines,
                    and budget.

                    <h4 className={styles.sectionH4}>Custom Design</h4>

                    Benefit from our custom design services, where we create scaffolding plans tailored to the unique
                    requirements of your project. Our designs prioritize efficiency, safety, and compliance with
                    industry standards.

                    <h3>Contact Us Today</h3>
<br />
                    Ready to elevate your construction projects with our top-notch scaffolding services? Contact us
                    today to discuss your project requirements or schedule a consultation. We look forward to being your
                    trusted partner in scaffolding solutions!
                </div>
            </div>
            <CtaBlock title={"Free consultation with exceptional quality"}
                      description={"Just one call away: +84 1102 2703"}
                      buttonText={"Get your consultation"} buttonLink={"about"}/>
        </>
    );
}

export default Services;