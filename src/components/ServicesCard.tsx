import React from 'react';
import styles from "../styles/ServicesCard.module.css";

interface ServicesCardParams {
    title: string;
    blockType: string;
}

const ServicesCard = ({title, blockType}: ServicesCardParams) => {
    return (
        <>
            <div className={styles.internalBlockItemWrapper}>
                <div className={`${styles.internalBlockItem} ${styles[blockType]}`}>
                    <div className={styles.blockTitle}>
                        <div className={styles.divider}></div>
                        <div className={styles.title}>{title}</div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default ServicesCard;