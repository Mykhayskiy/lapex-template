import React from "react";
import HeroSection from "../components/HeroSection";
import BlockSection from "../components/BlockSection";
import BlockAchievements from "../components/BlockAchievements";
import CtaBlock from "../components/CtaBlock";
import TextBlock from "../components/TextBlock";

const Home = () => {
    return (
        <>
            <HeroSection/>
            <TextBlock description={<div>
                Scaffolding is a temporary structure erected to support workers and materials during the construction, maintenance, or repair of buildings and other structures.
                It provides a safe and stable platform for workers to perform tasks at elevated heights, such as installing or repairing facades, windows, or roofs.<br />
                Scaffolding is a crucial component in the construction industry, offering a secure working environment that minimizes the risks associated with working at heights.<br />
                Scaffolding services encompass a range of offerings related to the design, erection, maintenance, and dismantling of scaffolding structures.
            </div>} />

            <BlockSection title={"Services"} sectionColor={"grey"} blockType={"services"} />

            <BlockAchievements title={""} sectionColor={"white"} blockType={"achievements"} />

            <CtaBlock title={"Free consultation with exceptional quality"}
                      description={"Just one call away: +84 1102 2703"}
                      buttonText={"Get your consultation"} buttonLink={"about"} />
        </>
    );
}

export default Home;