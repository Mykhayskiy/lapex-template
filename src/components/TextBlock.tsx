import React from "react";
import styles from "../styles/TextBlock.module.css";

interface TextBlockProps {
    description: any;
}

const TextBlock = ({description}: TextBlockProps) => {
    return (
        <>
            <div className={styles.TextBlockDescription}>
            {description}
            </div>
        </>
    );
}

export default TextBlock;