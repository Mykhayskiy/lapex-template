import React, { useEffect } from "react";
import {Link, NavLink} from "react-router-dom";
import styles from "../styles/MainMenu.module.css";

const MainMenu = () => {
    function handleToggleButtonClick() {
        const navbarLinks = document.getElementsByClassName(styles.navbarLinks)[0];
        navbarLinks.classList.toggle(styles.active);
    }

    return(
        <>
        <nav id={styles.mainMenu}>
            <div className={styles.navbarTitle}><a href={"/"}><img src={'/images/logoLapex.svg'} width={'300px'}/></a></div>
            <a href={'#'} className={styles.toggleButton} onClick={handleToggleButtonClick}>
                <span className={styles.bar}></span>
                <span className={styles.bar}></span>
                <span className={styles.bar}></span>
            </a>
            <div className={styles.navbarLinks}>
                <ul>
                    <li><NavLink to={"/"}>Home</NavLink></li>
                    <li><NavLink to={"/about"}>About</NavLink></li>
                    <li><NavLink to={"/services"}>Services</NavLink></li>
                    <li><NavLink to={"/contacts"}>Contacts</NavLink></li>
                </ul>
            </div>
        </nav>
        </>
    );
}
export default MainMenu;