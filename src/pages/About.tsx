import React from "react";
import styles from "../styles/page.module.css";
import CtaBlock from "../components/CtaBlock";
import { Helmet } from "react-helmet";

const About = () => {
    return (
        <>
            <Helmet>
                <title>About | Lapex - Scaffolding</title>
                <meta name="description" content="About" />
            </Helmet>

            <div className={styles.section}>
                <div className={styles.pageInner}>
                    <h1 className={styles.pageTitle}>About</h1>
                    Welcome to Lapex Inc., Your Trusted Partner in Scaffolding Services!
                    <br /><br />
                    At Lapex Inc., we take pride in being a leading provider of comprehensive scaffolding solutions.
                    <br />With years of expertise in the industry, we have established ourselves as a reliable partner for all
                    your scaffolding needs.<br />Our commitment to safety, quality, and excellence sets us apart in the
                    competitive landscape.
                    <h3>Our Mission</h3>
                    Our mission is to ensure the safety and success of your construction projects by delivering
                    top-notch scaffolding services. We aim to be the go-to partner for businesses seeking a seamless and
                    secure working environment at elevated heights.
                    <h3>What We Offer</h3>
                    <h4 className={styles.sectionH4}>Scaffolding Installation</h4>

                    Our skilled and experienced team specializes in the precise and efficient installation of
                    scaffolding structures. We understand the unique requirements of each project and tailor our
                    services to provide optimal support for your construction, maintenance, or repair tasks.

                    <h4 className={styles.sectionH4}>Scaffolding Removal</h4>

                    Once your project is complete, count on us for swift and safe scaffolding removal. Our trained
                    professionals will dismantle the structures with precision, ensuring a smooth transition and minimal
                    disruption to your operations.

                    <h3>Why Choose Us</h3>

                    <h4 className={styles.sectionH4}>Safety First:</h4> We prioritize the safety of our clients and workers. Our scaffolding solutions comply
                    with the highest safety standards, giving you peace of mind during your projects.

                    <h4 className={styles.sectionH4}>Quality Assurance:</h4> We are committed to delivering scaffolding structures of the highest quality. Our
                    materials and installations undergo rigorous checks to meet industry standards.

                        <h4 className={styles.sectionH4}>Experienced Team:</h4> Our team comprises skilled and certified professionals with extensive experience
                    in scaffolding installation and removal. Trust us to handle your project with expertise.

                            <h4 className={styles.sectionH4}>Custom Solutions:</h4> We understand that every project is unique. Our team works closely with clients to
                    design customized scaffolding solutions that align with their specific needs and requirements.

                    <h3>Get in Touch</h3>

                    Ready to elevate your construction projects with safe and reliable scaffolding services?<br />Contact us
                    today to discuss your requirements.<br />We look forward to partnering with you for success!
                </div>
            </div>
            <CtaBlock title={"Free consultation with exceptional quality"}
                      description={"Just one call away: +84 1102 2703"}
                      buttonText={"Get your consultation"} buttonLink={"about"} />
        </>
    );
}

export default About;