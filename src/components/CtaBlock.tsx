import React from "react";
import styles from "../styles/CtaBlock.module.css";

interface CtaBlockParams {
    title: string;
    description: string;
    buttonText: string;
    buttonLink: string;
}

const CtaBlock = ({title, description, buttonText, buttonLink}: CtaBlockParams) => {
    return (
        <>
            <div className={styles.catBlockSection}>
                <div className={`${styles.ctaBlockWrapper}`}>
                    <div className={styles.ctaBlockInner}>
                        <div className={styles.ctaBlockLeft}>
                            <div className={styles.ctaBlockTitle}>{title}</div>
                            <div className={styles.ctaBlockDescription}>{description}</div>
                        </div>
                        <div className={styles.ctaButton}>
                            <a className={""} href={"/contacts"}>{buttonText}</a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CtaBlock;